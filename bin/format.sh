#!/bin/bash

PROJECT_DIRS=(./source ./include ./test)
EXCLUDED_FILE_NAMES=("unity*")

check_only=0

while getopts :e flag
do
    case "${flag}" in
        e) check_only=1;;
        *) echo "${flag} is not a valid flag."
    esac
done

if [[ ${check_only} == 1 ]]
then
  FORMATTER_FLAGS=(-style=file -dry-run -Werror)
else
  FORMATTER_FLAGS=(-style=file -i)
fi

set -x
find "${PROJECT_DIRS[@]}" -not -name "${EXCLUDED_FILE_NAMES[@]}" -type f -exec clang-format "${FORMATTER_FLAGS[@]}" {} +
RESULT=$?
{ set +x; } 2>/dev/null  # To redirect the output of disabling the verbose attribute.

exit ${RESULT}
# If this fails you can try formatting your code with this command:
# > find ./source ./include ./test -type f -exec clang-format -style=file -i {} +
# Or run the script:
# > ./bin/format.sh