#!/bin/bash

test_executables=(build/test/out/test_*.out)

for test_executable in ${test_executables[*]}
do
  printf "\nRunning Test Executable %s without memory check...\n" "${test_executable}"
  ${test_executable};
done
