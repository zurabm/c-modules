#!/bin/bash

VALGRIND_FLAGS=(--leak-check=full --track-origins=yes)

test_executables=(build/test/out/test_*.out)

for test_executable in ${test_executables[*]}
do
  printf "\nRunning Test Executable %s with memory check...\n" "${test_executable}"

  set -x
  valgrind "${VALGRIND_FLAGS[@]}" "${test_executable}"
  { set +x; } 2>/dev/null  # To redirect the output of disabling the verbose attribute.
done