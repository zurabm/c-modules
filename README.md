# C Modules

This is a small library of convenient C modules that I frequently need to use in various projects. I've decided
to keep them in one place, so that I can reuse them and maintain them easily.

___

### Data Structures

This library contains modules for the following data structures:
* Generic Hash Map
* Generic Linked List

### Tokenizer

This library contains a module for a simple string tokenizer implementation.

### String Parser

This library contains a module for a string parser. It can be used, for example, to parse command line arguments.

### Data Tabulator

This library contains a module for a tabulator. It can be used to present data in an easily readable format, when
writing a command line application.

### Waveform Generator

This library contains a module for generating several types of waveforms. This can be used in conjunction with audio
modules for embedded systems. The following types of waveforms are supported:
* Sine Wave
* Triangular Wave
* Saw-tooth Wave
* Rectangular Wave