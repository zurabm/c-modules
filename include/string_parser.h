#ifndef C_MODULES_CONVERTERS_H
#define C_MODULES_CONVERTERS_H

#include <inttypes.h>

/**
 * @brief Converts the specified string to an unsigned integer.
 *
 * @param string The string containing the integer.
 * @param length The length of the string.
 * @returns The integer value of the string.
 */
uint32_t string_to_unsigned_integer(const char *string, uint32_t length);

/**
 * @brief Checks that the specified string is an unsigned integer.
 *
 * @param string The string containing the integer.
 * @param length The length of the string.
 * @returns Non-zero value if the string is an unsigned integer, zero otherwise.
 */
uint8_t string_is_unsigned_integer(const char *string, uint32_t length);

#endif /* C_MODULES_CONVERTERS_H */
