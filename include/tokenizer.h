#ifndef C_MODULES_TOKENIZER_H
#define C_MODULES_TOKENIZER_H

#include "class.h"

#include <inttypes.h>

class(tokenizer_t);

/**
 * @brief Creates and initializes a tokenizer with the specified character array.
 *
 * @param string The string to be tokenized.
 * @param length The number of characters present in the string.
 * @param delimiter The delimiter character to be used to tokenize the string.
 * @returns A reference to the newly allocated tokenizer.
 */
tokenizer_t tokenizer_create(const char *string, uint32_t length, char delimiter);

/**
 * @brief Checks if this tokenizer has any tokens left.
 *
 * @param tokenizer A reference to the tokenizer struct.
 * @returns A Non-zero value if the tokenizer has at least one more token, 0 otherwise.
 */
uint8_t tokenizer_has_next(tokenizer_t tokenizer);

/**
 * @brief Retrieves the next token from this tokenizer.
 *
 * @param tokenizer A reference to the tokenizer struct.
 * @param token_length A reference to an unsigned integer that will be populated with the
 * length of the token [Optional].
 * @returns A reference to the retrieved token (delimiter characters will be skipped).
 */
const char *tokenizer_get_next(tokenizer_t tokenizer, uint32_t *token_length);

/**
 * @brief Deallocates any resources used by the specified tokenizer.
 *
 * @param tokenizer A reference to the tokenizer struct.
 * @returns void.
 */
void tokenizer_destroy(tokenizer_t tokenizer);

#endif // C_MODULES_TOKENIZER_H
