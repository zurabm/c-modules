#ifndef C_MODULES_STRING_WRAPPER_H
#define C_MODULES_STRING_WRAPPER_H

#include "class.h"

#include <inttypes.h>

/**
 * @brief Wraps the specified string literal into a string_t object.
 */
#define as_string(literal) string_create(literal, sizeof(literal) - 1)

// === STRING WRAPPER ================================================================ //

class(string_t);

/**
 * @brief Creates a string wrapper for specified character array.
 *
 * @param characters A reference to the character array.
 * @param length The length of the character array.
 * @returns A reference to the newly created string wrapper.
 */
string_t string_create(const char *characters, uint32_t length);

/**
 * @brief Getter for the character array of the string.
 *
 * @param string A reference to the string wrapper.
 * @returns A reference to the start of the string.
 */
const char *string_get_characters(const_string_t string);

/**
 * @brief Getter for the length of the string.
 *
 * @param string A reference to the string wrapper.
 * @returns The length of the string.
 */
uint32_t string_get_length(const_string_t string);

/**
 * @brief Compares the two string wrappers. It's just like a regular string compare, but
 * the lengths are known in advance.
 *
 * @param string_1 The first string to be compared.
 * @param string_2 The second string to be compared.
 * @returns -1 if string_1 < string_2, 1 if string_1 > string_2, 0 if string_1 ==
 * string_2.
 */
uint8_t string_compare(const_string_t string_1, const_string_t string_2);

/**
 * @brief Returns the hash for the specified string.
 *
 * @param string A reference to the string wrapper.
 * @returns The hash for the specified string.
 */
uint32_t string_hash(const_string_t string);

/**
 * @brief Slices the specified string wrapper and leaves only the characters in the given
 * range.
 *
 * @param string A reference to the string wrapper.
 * @param start The start index of the character slice.
 * @param end The end index of the character slice, exclusive (if 0, the end of the string
 * will be taken).
 * @returns A reference to the same string object.
 *
 * @note This method mutates the passed string wrapper and does not create a new one.
 */
string_t string_slice(string_t string, uint32_t start, uint32_t end);

/**
 * @brief Deallocates any resources used by the specified string wrapper.
 *
 * @param string A reference to the string wrapper.
 * @returns void.
 */
void string_destroy(string_t string);

#endif // C_MODULES_STRING_WRAPPER_H
