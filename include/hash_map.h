#ifndef C_MODULES_HASH_MAP_H
#define C_MODULES_HASH_MAP_H

#include "class.h"

#include <inttypes.h>

/**
 * @brief The hash function should return a deterministic hash for the provided object.
 */
typedef uint32_t (*hash_function)(const void *);

/**
 * @brief The compare function should return a non-zero value if the specified objects are
 * equal, zero otherwise.
 */
typedef uint8_t (*compare_function)(const void *, const void *);

/**
 * @brief The free function should deallocate any resources used by the provided object.
 */
typedef void (*free_function)(void *);

// === HASH MAP ENTRY ================================================================ //

class(hash_map_entry_t);

/**
 * @brief Retrieves the key from the specified hash map entry.
 *
 * @param entry A reference to the hash map entry struct.
 * @returns A reference to the key of the entry.
 */
void *hash_map_entry_key(const_hash_map_entry_t entry);

/**
 * @brief Retrieves the value from the specified hash map entry.
 *
 * @param entry A reference to the hash map entry struct.
 * @returns A reference to the value of the entry.
 */
void *hash_map_entry_value(const_hash_map_entry_t entry);

/**
 * @brief Deallocates any memory used by the specified hash map entry.
 *
 * @param entry A reference to the hash map entry struct.
 * @param free_key If non-zero, the key associated with this entry will also be freed.
 * @param free_value If non-zero, the value associated with this entry will also be freed.
 * @returns void.
 */
void hash_map_entry_destroy(hash_map_entry_t entry, uint8_t free_key, uint8_t free_value);

// === HASH MAP ====================================================================== //

class(hash_map_t);

/**
 * @brief Creates an empty hash map. The references to keys and values must be used.
 *
 * @param size The initial number of elements the hash map should be able to store.
 * @param compare The compare function used for comparing keys.
 * @param hash The hash function used for calculating hash values for keys.
 * @param key_free The free function used for disposing keys (optional).
 * @param value_free The free function used for disposing values (optional).
 * @returns A reference to the hash map.
 */
hash_map_t hash_map_create(uint32_t size, compare_function compare, hash_function hash,
                           free_function key_free, free_function value_free);

/**
 * @brief Returns the number of keys currently in the hash map.
 *
 * @param hash_map A reference to the hash map struct.
 * @returns The number of keys currently present.
 */
uint32_t hash_map_size(hash_map_t hash_map);

/**
 * @brief Checks if the specified key is present in this hash map.
 *
 * @param hash_map A reference to the hash map struct.
 * @param key A reference to the key.
 * @returns A non-zero value if the key is present, zero otherwise.
 */
uint8_t hash_map_has_key(hash_map_t hash_map, const void *key);

/**
 * @brief Returns the value associated with the specified key.
 *
 * @param hash_map A reference to the hash map struct.
 * @param key A reference to the key.
 * @returns The value associated with this key.
 */
const void *hash_map_get_value(hash_map_t hash_map, const void *key);

/**
 * @brief Returns the value associated with the specified key.
 *
 * @param hash_map A reference to the hash map struct.
 * @param key A reference to the key.
 * @param value A reference to the value.
 * @returns void.
 */
void hash_map_put_value(hash_map_t hash_map, void *key, void *value);

/**
 * @brief Removes the specified key from the hash map.
 *
 * @param hash_map A reference to the hash map struct.
 * @param key A reference to the key.
 * @returns A reference to the hash map entry associated with this key.
 */
hash_map_entry_t hash_map_remove_key(hash_map_t hash_map, const void *key);

/**
 * @brief Deallocates any memory used by the specified hash map.
 *
 * @param hash_map A reference to the hash map struct.
 * @returns void.
 */
void hash_map_destroy(hash_map_t hash_map);

#endif // C_MODULES_HASH_MAP_H
