#ifndef C_MODULES_WAVEFORM_H
#define C_MODULES_WAVEFORM_H

#include "class.h"

#include <inttypes.h>

typedef enum { SINE, TRIANGULAR, RECTANGULAR, SAW_TOOTH } waveform_type_t;

class(waveform_generator_t);

/**
 * @brief Creates a waveform generator, that can be used to create data matching the given
 * specifications.
 *
 * @param type The shape of the waveform to be generated (Enum value).
 * @param frequency The frequency of the waveform to be generated in Hz.
 * @param length_ms The length of the waveform to be generated in ms.
 * @param sampling_rate The sampling rate to be used on the generated waveform to play it
 * samples / second.
 * @returns A reference to the waveform generator.
 */
waveform_generator_t waveform_generator_create(waveform_type_t type, double frequency,
                                               double length_ms, double sampling_rate);

/**
 * @brief Generates the waveform data and writes it to the specified buffer.
 *
 * @param generator A reference to the waveform generator.
 * @param amplitude The absolute maximum value that the waveform data can have.
 * @param buffer The buffer that the waveform data should be written to.
 * @param buffer_length The length of the buffer (if the data does not fit into the
 * buffer, the buffer will not overflow).
 * @returns The full length of the waveform data.
 */
uint32_t waveform_generator_generate(waveform_generator_t generator, double amplitude,
                                     float *buffer, uint32_t buffer_length);

/**
 * @brief Deallocates any memory used by the specified waveform generator.
 *
 * @param generator A reference to the waveform generator.
 * @returns void.
 */
void waveform_generator_destroy(waveform_generator_t generator);

#endif /* C_MODULES_WAVEFORM_H */
