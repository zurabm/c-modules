#ifndef C_MODULES_WAV_H
#define C_MODULES_WAV_H

#include "class.h"

#include <inttypes.h>

typedef struct wav_t {
    const char riff[4];
    const uint32_t file_size;
    const char wave[4];
    const char fmt[4];
    const uint32_t format_data_length;
    const uint16_t format_type;
    const uint16_t channel_count;
    const uint32_t sample_rate;
    const uint32_t byte_rate;
    const uint16_t block_align;
    const uint16_t bits_per_sample;
    const char data[4];
    const uint32_t data_size;
    uint8_t const *data_bytes;
} * wav_t;

/**
 * @brief Parses the audio data bytes in .wav format.
 *
 * @param wav_bytes The raw bytes for the audio data.
 * @param length The number of data bytes.
 * @returns A reference to the wav object containing the parsed data.
 */
wav_t wav_create(const uint8_t *wav_bytes, uint32_t length);

/**
 * @brief Deallocates any memory used by the specified wav object.
 *
 * @param wav A reference to the wav object.
 * @returns void.
 */
void wav_destroy(wav_t wav);

#endif // C_MODULES_WAV_H
