#ifndef C_MODULES_MAPPING_H
#define C_MODULES_MAPPING_H

/**
 * @brief Creates a key-value mapping struct for the specified types.
 *
 * @param key_type The type of the key to be used.
 * @param value_type The type of the value to be used.
 *
 * @note The key and value types should be a single word (no spaces), otherwise the
 * generated name for this unique pair will not be compilable.
 */
#define mapping(key_type, value_type)                                                    \
    typedef struct {                                                                     \
        key_type key;                                                                    \
        value_type value;                                                                \
    } key_type##_to_##value_type##_mapping_t

/**
 * @brief Generates the name of the mapping struct that would be created if mapping() was
 * used.
 *
 * @param key_type The type of the key.
 * @param value_type The type of the value.
 */
#define mapping_of(key_type, value_type) key_type##_to_##value_type##_mapping_t

/**
 * @brief Creates a single mapping entry for the mapping struct created by mapping().
 *
 * @param as_key The value to be used as the key of the mapping.
 * @param as_value The value to be used as the value of the mapping.
 */
#define map(as_key, as_value)                                                            \
    { .key = as_key, .value = as_value }

#endif // C_MODULES_MAPPING_H
