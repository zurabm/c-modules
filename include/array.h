#ifndef C_MODULES_ARRAY_H
#define C_MODULES_ARRAY_H

/**
 * @brief Calculates the size of an array created at compile time.
 *
 * @param array The array for which the size should be calculated.
 */
#define array_size(array) (sizeof(array) / sizeof(array[0]))

#endif // C_MODULES_ARRAY_H
