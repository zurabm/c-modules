#ifndef C_MODULES_LINKED_LIST_H
#define C_MODULES_LINKED_LIST_H

#include "class.h"

#include <inttypes.h>

/**
 * @brief The compare function should return a non-zero value if the specified objects are
 * equal, zero otherwise.
 */
typedef uint8_t (*compare_function)(const void *, const void *);

/**
 * @brief The free function should deallocate any resources used by the provided object.
 */
typedef void (*free_function)(void *);

// === LINKED LIST =================================================================== //

class(linked_list_t);

/**
 * @brief Creates an empty linked list.
 *
 * @param compare The compare function used for comparing elements (optional).
 * @param free The free function used for disposing elements of this list (optional).
 * @returns A reference to the linked list.
 */
linked_list_t linked_list_create(compare_function compare, free_function free);

/**
 * @brief Adds the specified element to the linked list.
 *
 * @param linked_list A reference to the linked list struct.
 * @param element A reference to the element to be added.
 * @returns void.
 */
void linked_list_add(linked_list_t linked_list, void *element);

/**
 * @brief Returns the number of elements currently in this linked list.
 *
 * @param linked_list A reference to the linked list struct.
 * @returns The number of elements currently present.
 */
uint32_t linked_list_size(linked_list_t linked_list);

/**
 * @brief Searches for the specified element in the linked list. A compare function must
 * be specified to use this feature.
 *
 * @param linked_list A reference to the linked list struct.
 * @param element A reference to the element to be added.
 * @returns The reference to the element if it is present, otherwise null.
 */
void *linked_list_find(linked_list_t linked_list, const void *element);

/**
 * @brief Removes the specified element from this linked list.
 *
 * @param linked_list A reference to the linked list struct.
 * @param element A reference to the element to be removed.
 * @returns A reference to the removed element.
 */
void *linked_list_remove(linked_list_t linked_list, const void *element);

/**
 * @brief Deallocates any memory used by the specified linked list.
 *
 * @param linked_list A reference to the linked list struct.
 * @returns void.
 */
void linked_list_destroy(linked_list_t linked_list);

// === LINKED LIST ITERATOR ========================================================== //

class(linked_list_iterator_t);

/**
 * @brief Returns a new iterator to this linked list.
 *
 * @param linked_list A reference to the linked list struct.
 * @returns A linked list iterator that can be used to access the elements of this linked
 * list.
 */
linked_list_iterator_t linked_list_iterator_create(linked_list_t linked_list);

/**
 * @brief Checks if the iterator has any more elements left.
 *
 * @param iterator A reference to the iterator.
 * @returns A non-zero value if there are elements left to iterate over, zero otherwise.
 */
uint8_t linked_list_iterator_has_next(linked_list_iterator_t iterator);

/**
 * @brief Retrieves the next element from the iterator.
 *
 * @param iterator A reference to the iterator.
 * @returns A reference to the next element in the iterator.
 */
void *linked_list_iterator_get_next(linked_list_iterator_t iterator);

/**
 * @brief Deallocates any memory used by the specified iterator.
 *
 * @param iterator A reference to the iterator.
 * @returns void.
 */
void linked_list_iterator_destroy(linked_list_iterator_t iterator);

#endif // C_MODULES_LINKED_LIST_H
