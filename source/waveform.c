#include "waveform.h"

#include "array.h"
#include "assertion.h"
#include "mapping.h"

#include <math.h>

#define MSECONDS_PER_SECOND 1000
#define PRECISION_FACTOR    1000000

// === MACRO DEFINITIONS ============================================================= //

/**
 * @brief Truncates the specified value according to PRECISION_FACTOR.
 */
#define truncate(value) trunc(value *PRECISION_FACTOR) / PRECISION_FACTOR

// === STRUCT DEFINITIONS ============================================================ //

typedef double (*wave_function)(double argument, double frequency);

struct waveform_generator_t {
    const wave_function wave;
    const double frequency;
    const double length_ms;
    const double sampling_rate;
};

mapping(waveform_type_t, wave_function);

// === PRIVATE FUNCTIONS ============================================================= //

/**
 * @brief Wave function for generating a sine wave of the specified frequency.
 */
static double sine_wave(double argument, double frequency) {
    return sin(2 * M_PI * frequency * argument);
}

/**
 * @brief Wave function for generating a triangular wave of the specified frequency.
 */
static double triangular_wave(double argument, double frequency) {
    double period = 1 / frequency;
    double half_period = period / 2;
    double argument_remainder = fmod(argument, period);

    if (argument_remainder < half_period) {
        return argument_remainder / half_period;
    } else {
        return 2 - argument_remainder / half_period;
    }
}

/**
 * @brief Wave function for generating a saw-tooth wave of the specified frequency.
 */
static double saw_tooth_wave(double argument, double frequency) {
    double period = 1 / frequency;
    double argument_remainder = fmod(argument, period);

    return argument_remainder / period;
}

/**
 * @brief Wave function for generating a saw-tooth wave of the specified frequency.
 */
static double rectangular_wave(double argument, double frequency) {
    double period = 1 / frequency;
    double argument_remainder = fmod(argument, period);

    return argument_remainder < (period / 2);
}

// === PRIVATE CONSTANTS ============================================================= //

static const mapping_of(waveform_type_t, wave_function) WAVE_FUNCTIONS[] = {
    map(SINE, sine_wave), map(TRIANGULAR, triangular_wave),
    map(SAW_TOOTH, saw_tooth_wave), map(RECTANGULAR, rectangular_wave)};

/**
 * @brief Returns the wave function of the specified type.
 */
static wave_function get_wave_function(waveform_type_t type) {
    for (uint32_t i = 0; i < array_size(WAVE_FUNCTIONS); i++) {
        if (type == WAVE_FUNCTIONS[i].key) {
            return WAVE_FUNCTIONS[i].value;
        }
    }
    ASSERT(!"A wave function for the given type does not exist.");
}

// === IMPLEMENTATIONS =============================================================== //

waveform_generator_t waveform_generator_create(waveform_type_t type, double frequency,
                                               double length_ms, double sampling_rate) {
    ASSERT(frequency > 0);
    ASSERT(length_ms > 0);
    ASSERT(sampling_rate > 0);

    waveform_generator_t generator = allocate(waveform_generator_t);

    initialize_const(wave_function, generator->wave, get_wave_function(type));
    initialize_const(double, generator->frequency, frequency);
    initialize_const(double, generator->length_ms, length_ms);
    initialize_const(double, generator->sampling_rate, sampling_rate);

    return generator;
}

uint32_t waveform_generator_generate(waveform_generator_t generator, double amplitude,
                                     float *buffer, uint32_t buffer_length) {
    ASSERT(generator != NULL);
    ASSERT(amplitude > 0);
    ASSERT(buffer != NULL);

    double length_s = generator->length_ms / MSECONDS_PER_SECOND;

    uint32_t n_points = (uint32_t)(length_s * generator->sampling_rate);
    if (n_points > buffer_length) {
        n_points = buffer_length;
    }

    for (uint32_t i = 0; i < n_points; i++) {
        buffer[i] = (float)truncate(
            amplitude *
            generator->wave(i / generator->sampling_rate, generator->frequency));
    }
    return n_points;
}

void waveform_generator_destroy(waveform_generator_t generator) {
    ASSERT(generator != NULL);
    free(generator);
}
