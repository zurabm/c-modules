#include "wav.h"

#include "assertion.h"

#include <memory.h>

#define HEADER_SIZE  44
#define BITS_IN_BYTE 8

// === PRIVATE FUNCTIONS ============================================================= //

/**
 * @brief Parses the specified little endian number so that it is interpreted the same,
 * regardless of the endianness of the current machine.
 */
static uint32_t parse_little_endian_integer(const uint8_t *bytes, uint8_t byte_count) {
    ASSERT(bytes != NULL);
    ASSERT(0 < byte_count && byte_count <= sizeof(uint32_t));

    uint32_t result = 0;

    for (uint8_t i = 0; i < byte_count; i++) {
        result |= bytes[i] << (BITS_IN_BYTE * i);
    }
    return result;
}

// === IMPLEMENTATIONS =============================================================== //

wav_t wav_create(const uint8_t *wav_bytes, uint32_t length) {
    ASSERT(length >= HEADER_SIZE);

    wav_t wav = allocate(wav_t);
    memcpy(wav, wav_bytes, HEADER_SIZE);

    initialize_const(
        uint32_t, wav->file_size,
        parse_little_endian_integer((uint8_t *)&wav->file_size, sizeof(uint32_t)));
    initialize_const(
        uint32_t, wav->sample_rate,
        parse_little_endian_integer((uint8_t *)&wav->sample_rate, sizeof(uint32_t)));
    initialize_const(
        uint32_t, wav->data_size,
        parse_little_endian_integer((uint8_t *)&wav->data_size, sizeof(uint32_t)));

    uint8_t *data_bytes = malloc(wav->data_size);
    memcpy(data_bytes, wav_bytes + HEADER_SIZE, wav->data_size);

    initialize_const(uint8_t *, wav->data_bytes, data_bytes);

    return wav;
}

void wav_destroy(wav_t wav) {
    ASSERT(wav != NULL);

    free((uint8_t *)wav->data_bytes);
    free(wav);
}
