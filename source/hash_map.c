#include "hash_map.h"

#include "assertion.h"
#include "linked_list.h"

#include <memory.h>

#define DUTY_FACTOR   2
#define EXTEND_FACTOR 2

// === STRUCT DEFINITIONS ============================================================ //

typedef struct pair_t {
    void *key;
    void *value;
    compare_function key_compare;
} pair_t;

struct hash_map_entry_t {
    void *const key;
    void *const value;
    const free_function key_free;
    const free_function value_free;
};

struct hash_map_t {
    const compare_function compare;
    const hash_function hash;
    const free_function key_free;
    const free_function value_free;
    linked_list_t *buckets;
    uint32_t n_buckets;
    uint32_t n_pairs;
};

// === PRIVATE FUNCTIONS ============================================================= //

/**
 * @brief Creates a hash map entry for the specified hash map, key and value.
 */
static hash_map_entry_t hash_map_entry_create(hash_map_t hash_map, void *key,
                                              void *value) {
    hash_map_entry_t entry = allocate(hash_map_entry_t);
    initialize_const(void *, entry->key, key);
    initialize_const(void *, entry->value, value);
    initialize_const(free_function, entry->key_free, hash_map->key_free);
    initialize_const(free_function, entry->value_free, hash_map->value_free);

    return entry;
}

/**
 * @brief Comparison function to compare pairs with each other based on their keys.
 */
static uint8_t pair_compare(const void *pair_1_ref, const void *pair_2_ref) {
    ASSERT(pair_1_ref != NULL);
    ASSERT(pair_2_ref != NULL);

    const pair_t *pair_1 = pair_1_ref;
    const pair_t *pair_2 = pair_2_ref;

    return pair_1->key_compare(pair_1->key, pair_2->key);
}

/**
 * @brief Frees the specified pair and the key-value contained in it.
 */
static void pair_free(pair_t *pair, free_function key_free, free_function value_free) {
    ASSERT(pair != NULL);

    if (key_free != NULL) {
        key_free(pair->key);
    }
    if (value_free != NULL) {
        value_free(pair->value);
    }

    free(pair);
}

/**
 * @brief Calculates the hash for the specified hash map from the specified key.
 */
static uint32_t get_hash(hash_map_t hash_map, const void *key) {
    return hash_map->hash(key) % hash_map->n_buckets;
}

/**
 * @brief Creates a new pair_t struct from the specified key and value.
 */
static pair_t *create_pair(void *key, void *value, compare_function key_compare) {
    pair_t *pair = allocate(pair_t);

    pair->key = key;
    pair->value = value;
    pair->key_compare = key_compare;

    return pair;
}

/**
 * @brief Finds and returns the bucker associated ith the specified key, or null if one
 * doesn't exist.
 */
static linked_list_t find_bucket(hash_map_t hash_map, const void *key) {
    return hash_map->buckets[get_hash(hash_map, key)];
}

/**
 * @brief Finds and returns the pair associated with the specified key, or null if one
 * doesn't exist.
 */
static pair_t *find_pair(hash_map_t hash_map, const void *key) {
    pair_t pair = {.key = (void *)key, .key_compare = hash_map->compare};
    linked_list_t bucket = find_bucket(hash_map, key);

    return bucket == NULL ? NULL : linked_list_find(bucket, &pair);
}

/**
 * @brief Creates a new bucket for the specified key in the given hash map.
 */
static void add_bucket(hash_map_t hash_map, void *key, void *value) {
    uint32_t hash = get_hash(hash_map, key);
    linked_list_t bucket = hash_map->buckets[hash];

    if (bucket == NULL) {
        hash_map->buckets[hash] = linked_list_create(pair_compare, NULL);
    }

    linked_list_add(hash_map->buckets[hash], create_pair(key, value, hash_map->compare));
    hash_map->n_pairs++;
}

/**
 * @brief Extends the array of buckets for the hash map and rehashes all the elements.
 */
static void extend_buckets(hash_map_t hash_map, uint32_t new_size) {
    linked_list_t *old_buckets = hash_map->buckets;
    uint32_t old_n_buckets = hash_map->n_buckets;

    linked_list_t *new_array = malloc(new_size * sizeof(linked_list_t));
    memset(new_array, 0, new_size * sizeof(linked_list_t));

    hash_map->buckets = new_array;
    hash_map->n_buckets = new_size;
    hash_map->n_pairs = 0;

    for (uint32_t i = 0; i < old_n_buckets; i++) {
        linked_list_t bucket = old_buckets[i];

        if (bucket == NULL) {
            continue;
        }

        linked_list_iterator_t iterator = linked_list_iterator_create(bucket);
        while (linked_list_iterator_has_next(iterator)) {
            pair_t *pair = linked_list_iterator_get_next(iterator);
            hash_map_put_value(hash_map, pair->key, pair->value);

            free(pair);
        }
        linked_list_iterator_destroy(iterator);
        linked_list_destroy(bucket);
    }
    free(old_buckets);
}

// === IMPLEMENTATIONS =============================================================== //

hash_map_t hash_map_create(uint32_t size, compare_function compare, hash_function hash,
                           free_function key_free, free_function value_free) {
    ASSERT(compare != NULL);
    ASSERT(hash != NULL);
    ASSERT(size > 0);

    hash_map_t hash_map = allocate(hash_map_t);

    initialize_const(compare_function, hash_map->compare, compare);
    initialize_const(hash_function, hash_map->hash, hash);
    initialize_const(free_function, hash_map->key_free, key_free);
    initialize_const(free_function, hash_map->value_free, value_free);

    hash_map->buckets = malloc(size * sizeof(linked_list_t));
    hash_map->n_buckets = size;
    hash_map->n_pairs = 0;

    memset(hash_map->buckets, 0, size * sizeof(linked_list_t));

    return hash_map;
}

uint32_t hash_map_size(hash_map_t hash_map) {
    ASSERT(hash_map != NULL);

    return hash_map->n_pairs;
}

uint8_t hash_map_has_key(hash_map_t hash_map, const void *key) {
    ASSERT(hash_map != NULL);

    return find_pair(hash_map, key) != NULL;
}

const void *hash_map_get_value(hash_map_t hash_map, const void *key) {
    ASSERT(hash_map != NULL);
    ASSERT(hash_map_has_key(hash_map, key));

    return find_pair(hash_map, key)->value;
}

void hash_map_put_value(hash_map_t hash_map, void *key, void *value) {
    ASSERT(hash_map != NULL);

    pair_t *pair = find_pair(hash_map, key);
    if (pair != NULL) {
        if (hash_map->value_free != NULL) {
            hash_map->value_free(pair->value);
        }
        pair->value = value;
    } else {
        if (hash_map->n_pairs >= hash_map->n_buckets / DUTY_FACTOR) {
            extend_buckets(hash_map, hash_map->n_buckets * EXTEND_FACTOR);
        }
        add_bucket(hash_map, key, value);
    }
}

hash_map_entry_t hash_map_remove_key(hash_map_t hash_map, const void *key) {
    ASSERT(hash_map_has_key(hash_map, key));

    pair_t target_pair = {.key = (void *)key, .key_compare = hash_map->compare};

    linked_list_t bucket = find_bucket(hash_map, key);
    pair_t *removed_pair = linked_list_remove(bucket, &target_pair);

    hash_map->n_pairs--;

    hash_map_entry_t entry = hash_map_entry_create(hash_map, removed_pair->key,
                                                   removed_pair->value);

    pair_free(removed_pair, NULL, NULL);

    return entry;
}

void hash_map_destroy(hash_map_t hash_map) {
    ASSERT(hash_map != NULL);

    for (uint32_t i = 0; i < hash_map->n_buckets; i++) {
        linked_list_t bucket = hash_map->buckets[i];

        if (bucket == NULL) {
            continue;
        }

        linked_list_iterator_t iterator = linked_list_iterator_create(bucket);
        while (linked_list_iterator_has_next(iterator)) {
            pair_t *pair = linked_list_iterator_get_next(iterator);
            pair_free(pair, hash_map->key_free, hash_map->value_free);
        }
        linked_list_iterator_destroy(iterator);
        linked_list_destroy(bucket);
    }

    free(hash_map->buckets);
    free(hash_map);
}

// === ENTRY IMPLEMENTATIONS ========================================================= //

void *hash_map_entry_key(const_hash_map_entry_t entry) {
    ASSERT(entry != NULL);

    return entry->key;
}

void *hash_map_entry_value(const_hash_map_entry_t entry) {
    ASSERT(entry != NULL);

    return entry->value;
}

void hash_map_entry_destroy(hash_map_entry_t entry, uint8_t free_key,
                            uint8_t free_value) {
    ASSERT(entry != NULL);

    if (free_key) {
        entry->key_free(entry->key);
    }
    if (free_value) {
        entry->value_free(entry->value);
    }

    free(entry);
}
