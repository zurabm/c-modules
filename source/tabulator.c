#include "tabulator.h"

#include "assertion.h"
#include "linked_list.h"

#include <memory.h>

#define WHITESPACE ' '

// === STRUCT DEFINITIONS ============================================================ //

struct tabulator_t {
    tabulator_row_t header;
    linked_list_t rows;
};

struct tabulator_row_t {
    const string_t *values;
    const uint32_t value_count;
};

struct tabulator_iterator_t {
    uint8_t header_next;
    tabulator_row_t header;
    linked_list_iterator_t iterator;
};

// === PRIVATE FUNCTIONS ============================================================= //

/**
 * @brief Creates a tabulator row from the specified values. If the values are specified
 * to be null, then this row will be a separator row.
 */
static tabulator_row_t tabulator_row_create(string_t *values, uint32_t value_count) {
    tabulator_row_t row = allocate(tabulator_row_t);

    initialize_const(string_t *, row->values, values);
    initialize_const(uint32_t, row->value_count, value_count);

    return row;
}

/**
 * @brief Deallocates any resources used by the specified row. This will include the
 * string values for each column, as well as the array holding the references to the
 * string values, unless it is a separator row.
 */
void tabulator_row_destroy(tabulator_row_t row) {
    ASSERT(row != NULL);

    if (row->values != NULL) {
        for (uint32_t i = 0; i < row->value_count; i++) {
            string_destroy(row->values[i]);
        }
        free((void *)row->values);
    }

    free(row);
}

// === IMPLEMENTATIONS =============================================================== //

tabulator_t tabulator_create(string_t *columns, uint32_t column_count) {
    ASSERT(columns != NULL);
    ASSERT(column_count > 0);

    tabulator_t tabulator = allocate(tabulator_t);
    tabulator->header = tabulator_row_create(columns, column_count);
    tabulator->rows = linked_list_create(NULL, (free_function)tabulator_row_destroy);

    return tabulator;
}

void tabulator_add_row(tabulator_t tabulator, string_t *values, uint32_t value_count) {
    ASSERT(tabulator != NULL);
    ASSERT(value_count > 0);

    linked_list_add(tabulator->rows, tabulator_row_create(values, value_count));
}

uint32_t tabulator_clear(tabulator_t tabulator) {
    ASSERT(tabulator != NULL);

    uint32_t row_count = linked_list_size(tabulator->rows);

    linked_list_destroy(tabulator->rows);
    tabulator->rows = linked_list_create(NULL, (free_function)tabulator_row_destroy);

    return row_count;
}

void tabulator_destroy(tabulator_t tabulator) {
    ASSERT(tabulator != NULL);

    tabulator_row_destroy(tabulator->header);
    linked_list_destroy(tabulator->rows);
    free(tabulator);
}

// === ROW IMPLEMENTATIONS =========================================================== //

void tabulator_row_print(tabulator_row_t row, char *buffer, uint32_t column_width,
                         char column_delimiter, char row_delimiter,
                         char cross_character) {
    ASSERT(row != NULL);
    ASSERT(buffer != NULL);
    ASSERT(column_width > 0);

    uint32_t offset = 0;

    // A row begins with a column delimiter.
    buffer[offset++] = column_delimiter;

    for (uint32_t i = 0; i < row->value_count; i++) {
        if (row->values == NULL) { // This is a separator row.
            memset(buffer + offset, row_delimiter,
                   column_width); // Adds horizontal delimiters.

            offset += column_width;

            buffer[offset++] = cross_character; // Adds cross character.
        } else {
            uint32_t space_width;
            uint32_t value_width = string_get_length(row->values[i]);

            if (value_width > column_width) {
                value_width = column_width;
                space_width = 0;
            } else {
                space_width = column_width - value_width;
            }

            // Print the value padded with whitespace.
            memcpy(buffer + offset, string_get_characters(row->values[i]), value_width);
            memset(buffer + offset + value_width, WHITESPACE, space_width);

            offset += column_width;

            buffer[offset++] = column_delimiter; // Adds regular column delimiter.
        }
    }

    // Adds final column delimiter, newline and string delimiter.
    buffer[offset - 1] = column_delimiter;
    buffer[offset] = '\n';
    buffer[offset + 1] = '\0';
}

// === ITERATOR IMPLEMENTATIONS ====================================================== //

tabulator_iterator_t tabulator_iterator_create(tabulator_t tabulator) {
    ASSERT(tabulator != NULL);

    tabulator_iterator_t iterator = allocate(tabulator_iterator_t);
    iterator->header_next = 1;
    iterator->header = tabulator->header;
    iterator->iterator = linked_list_iterator_create(tabulator->rows);

    return iterator;
}

uint8_t tabulator_iterator_has_next(tabulator_iterator_t iterator) {
    ASSERT(iterator != NULL);

    return iterator->header_next || linked_list_iterator_has_next(iterator->iterator);
}

tabulator_row_t tabulator_iterator_get_next(tabulator_iterator_t iterator) {
    ASSERT(iterator != NULL);

    if (iterator->header_next) {
        iterator->header_next = 0;
        return iterator->header;
    }

    return linked_list_iterator_get_next(iterator->iterator);
}

void tabulator_iterator_destroy(tabulator_iterator_t iterator) {
    ASSERT(iterator != NULL);

    linked_list_iterator_destroy(iterator->iterator);
    free(iterator);
}
