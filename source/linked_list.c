#include "linked_list.h"

#include "assertion.h"

#include <malloc.h>

// === STRUCT DEFINITIONS ============================================================ //

typedef struct linked_list_node_t *linked_list_node_t;

struct linked_list_t {
    compare_function compare;
    free_function free;
    linked_list_node_t head;
    uint32_t node_count;
};

struct linked_list_node_t {
    void *element;
    linked_list_node_t next;
};

struct linked_list_iterator_t {
    linked_list_node_t current;
};

// === PRIVATE FUNCTIONS ============================================================= //

/**
 * @brief Creates a new linked list node from the specified element reference.
 */
static linked_list_node_t create_node(void *element) {
    linked_list_node_t node = allocate(linked_list_node_t);
    node->element = element;
    node->next = NULL;

    return node;
}

/**
 * @brief Recursively deallocates memory used by this node and all subsequent nodes.
 */
static void destroy_node(linked_list_node_t node, free_function element_free) {
    if (node != NULL) {
        if (element_free != NULL) {
            element_free(node->element);
        }

        destroy_node(node->next, element_free);
        free(node);
    }
}

// === IMPLEMENTATIONS =============================================================== //

linked_list_t linked_list_create(compare_function compare, free_function free) {
    linked_list_t linked_list = allocate(linked_list_t);

    initialize_const(compare_function, linked_list->compare, compare);
    initialize_const(free_function, linked_list->free, free);

    linked_list->head = NULL;
    linked_list->node_count = 0;

    return linked_list;
}

void linked_list_add(linked_list_t linked_list, void *element) {
    ASSERT(linked_list != NULL);

    if (linked_list->head == NULL) {
        linked_list->head = create_node(element);
    } else {
        linked_list_node_t node = linked_list->head;

        while (node->next != NULL) {
            node = node->next;
        }

        node->next = create_node(element);
    }

    linked_list->node_count++;
}

uint32_t linked_list_size(linked_list_t linked_list) {
    ASSERT(linked_list != NULL);

    return linked_list->node_count;
}

void *linked_list_find(linked_list_t linked_list, const void *element) {
    ASSERT(linked_list != NULL);
    ASSERT(linked_list->compare != NULL);

    linked_list_node_t node = linked_list->head;

    while (node != NULL) {
        if (linked_list->compare(element, node->element) == 0) {
            return node->element;
        }

        node = node->next;
    }
    return NULL;
}

void *linked_list_remove(linked_list_t linked_list, const void *element) {
    ASSERT(linked_list_find(linked_list, element) != NULL);

    linked_list_node_t node = linked_list->head;

    if (node == NULL) {
        return NULL;
    }

    if (linked_list->compare(element, node->element) == 0) {
        void *removed_element = node->element;

        linked_list->head = node->next; // Remove the node from the list.
        free(node);                     // Free node.

        linked_list->node_count--;
        return removed_element;
    } else {
        for (linked_list_node_t next_node = node->next; next_node != NULL;
             node = next_node, next_node = node->next) {

            if (linked_list->compare(element, next_node->element) == 0) {
                void *removed_element = next_node->element;

                node->next = next_node->next; // Remove the node.
                free(next_node);              // Free node.

                linked_list->node_count--;
                return removed_element;
            }
        }
        return NULL;
    }
}

void linked_list_destroy(linked_list_t linked_list) {
    ASSERT(linked_list != NULL);

    destroy_node(linked_list->head, linked_list->free);
    free(linked_list);
}

// === ITERATOR IMPLEMENTATIONS ====================================================== //

linked_list_iterator_t linked_list_iterator_create(linked_list_t linked_list) {
    ASSERT(linked_list != NULL);

    linked_list_iterator_t iterator = allocate(linked_list_iterator_t);
    iterator->current = linked_list->head;

    return iterator;
}

uint8_t linked_list_iterator_has_next(linked_list_iterator_t iterator) {
    ASSERT(iterator != NULL);
    return iterator->current != NULL;
}

void *linked_list_iterator_get_next(linked_list_iterator_t iterator) {
    ASSERT(linked_list_iterator_has_next(iterator));

    void *element = iterator->current->element;
    iterator->current = iterator->current->next;

    return element;
}

void linked_list_iterator_destroy(linked_list_iterator_t iterator) {
    ASSERT(iterator != NULL);
    free(iterator);
}
