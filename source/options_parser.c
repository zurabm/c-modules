#include "options_parser.h"

#include "assertion.h"
#include "hash_map.h"
#include "string_wrapper.h"
#include "tokenizer.h"

#include <malloc.h>
#include <string.h>

#define INIT_OPTION_COUNT 4

#define is_option(token, option_marker) ((token)[0] == (option_marker))

// === STRUCT DEFINITIONS ============================================================ //

struct options_parser_t {
    hash_map_t options;
};

// === PRIVATE FUNCTIONS ============================================================= //

/**
 * @brief A simple wrapper for comparing strings. For use with a hash map.
 */
static uint8_t string_compare_wrapper(const void *string_1, const void *string_2) {
    return string_compare(string_1, string_2);
}

/**
 * @brief A simple wrapper for hashing strings. For use with a hash map.
 */
static uint32_t string_hash_wrapper(const void *string) {
    return string_hash(string);
}

/**
 * @brief A simple wrapper for freeing strings. For use with a hash map.
 */
static void string_destroy_wrapper(void *string) {
    return string_destroy(string);
}

/**
 * @brief Returns the next token from the tokenizer as a string.
 */
static string_t tokenizer_get_next_as_string(tokenizer_t tokenizer) {
    uint32_t token_length;
    const char *token = tokenizer_get_next(tokenizer, &token_length);

    return string_create(token, token_length);
}

/**
 * @brief Iterates over the specified options string and parses any valid options it can
 * find.
 */
static void parse_options(options_parser_t options_parser, const char *options,
                          uint32_t options_length, char option_marker, char delimiter) {
    tokenizer_t tokenizer = tokenizer_create(options, options_length, delimiter);

    while (tokenizer_has_next(tokenizer)) {
        string_t token = tokenizer_get_next_as_string(tokenizer);

        if (is_option(string_get_characters(token), option_marker)) {
            if (tokenizer_has_next(tokenizer)) {
                hash_map_put_value(options_parser->options, string_slice(token, 1, 0),
                                   tokenizer_get_next_as_string(tokenizer));
            } else {
                hash_map_put_value(options_parser->options, string_slice(token, 1, 0),
                                   string_create("", 0));
            }
        } else {
            string_destroy(token);
        }
    }
    tokenizer_destroy(tokenizer);
}

// === IMPLEMENTATIONS =============================================================== //

options_parser_t options_parser_create(const char *options, uint32_t options_length,
                                       char option_marker, char delimiter) {
    ASSERT(options != NULL);
    ASSERT(options_length >= 0);

    options_parser_t options_parser = allocate(options_parser_t);

    options_parser->options = hash_map_create(INIT_OPTION_COUNT, string_compare_wrapper,
                                              string_hash_wrapper, string_destroy_wrapper,
                                              string_destroy_wrapper);
    parse_options(options_parser, options, options_length, option_marker, delimiter);

    return options_parser;
}

uint8_t options_parser_has_option(options_parser_t options_parser,
                                  const char *option_name) {
    ASSERT(options_parser != NULL);
    ASSERT(option_name != NULL);

    string_t option_string = string_create(option_name, strlen(option_name));
    uint8_t has_option = hash_map_has_key(options_parser->options, option_string);

    string_destroy(option_string);

    return has_option;
}

const char *options_parser_get_option(options_parser_t options_parser,
                                      const char *option_name, uint32_t *value_length) {
    ASSERT(options_parser_has_option(options_parser, option_name));
    ASSERT(value_length != NULL);

    string_t option_string = string_create(option_name, strlen(option_name));
    const_string_t option_value = hash_map_get_value(options_parser->options,
                                                     option_string);

    string_destroy(option_string);

    *value_length = string_get_length(option_value);
    return string_get_characters(option_value);
}

void options_parser_destroy(options_parser_t options_parser) {
    ASSERT(options_parser != NULL);

    hash_map_destroy(options_parser->options);
    free(options_parser);
}
