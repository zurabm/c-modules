#include "string_wrapper.h"

#include "assertion.h"

#include <malloc.h>
#include <string.h>

// === STRUCT DEFINITIONS ============================================================ //

struct string_t {
    const char *characters;
    uint32_t length;
};

// === IMPLEMENTATIONS =============================================================== //

string_t string_create(const char *characters, uint32_t length) {
    ASSERT(characters != NULL);
    ASSERT(length >= 0);

    struct string_t *string = allocate(string_t);

    string->characters = characters;
    string->length = length;

    return string;
}

const char *string_get_characters(const_string_t string) {
    ASSERT(string != NULL);
    return string->characters;
}

uint32_t string_get_length(const_string_t string) {
    ASSERT(string != NULL);
    return string->length;
}

uint8_t string_compare(const_string_t string_1, const_string_t string_2) {
    ASSERT(string_1 != NULL);
    ASSERT(string_2 != NULL);

    if (string_1->length > string_2->length) {
        return 1;
    } else if (string_1->length < string_2->length) {
        return -1;
    } else {
        return strncmp(string_1->characters, string_2->characters, string_1->length);
    }
}

uint32_t string_hash(const_string_t string) {
    ASSERT(string != NULL);

    uint32_t hash = 5381;
    const char *characters = string->characters;

    for (int i = 0; i < string->length; i++) {
        hash = ((hash << 5) + hash) + characters[i]; /* hash * 33 + c */
    }

    return hash;
}

string_t string_slice(string_t string, uint32_t start, uint32_t end) {
    ASSERT(string != NULL);
    ASSERT(0 <= start && start < string->length);
    ASSERT((end == 0) || (start < end && end <= string->length));

    if (end == 0) {
        end = string->length;
    }

    string->characters += start;
    string->length = end - start;

    return string;
}

void string_destroy(string_t string) {
    ASSERT(string != NULL);
    free(string);
}