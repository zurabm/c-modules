#ifndef C_MODULES_TEST_H
#define C_MODULES_TEST_H

#define SET_UP            __attribute__((unused)) void setUp(void)
#define TEAR_DOWN         __attribute__((unused)) void tearDown(void)
#define TEST_SHOULD(name) void test_should_##name(void)

#endif // C_MODULES_TEST_H
