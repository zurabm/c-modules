#include "array.h"
#include "string_wrapper.h"
#include "tabulator.h"
#include "test.h"
#include "unity.h"

#include <memory.h>

TEST_FILE("string_wrapper.c")
TEST_FILE("linked_list.c")

#define COLUMN_WIDTH     16
#define COLUMN_DELIMITER '|'
#define ROW_DELIMITER    '-'
#define CROSS_CHARACTER  '+'

// === CONVENIENCE MACROS FOR THE TESTS ============================================== //

/**
 * @brief Allocates the specified array body dynamically. A reference to the array and its
 * length is stored in the passed parameters.
 */
#define allocate_dynamically(array, length, ...)                                         \
    do {                                                                                 \
        string_t __temp__[] = __VA_ARGS__;                                               \
        (length) = array_size(__temp__);                                                 \
        (array) = malloc(sizeof(__temp__));                                              \
        memcpy(array, __temp__, sizeof(__temp__));                                       \
    } while (0)

/**
 * @brief Convenient macro for creating a test expectation.
 */
#define EXPECT(...)                                                                      \
    const char *__t__[] = __VA_ARGS__;                                                   \
    do {                                                                                 \
        expect(__t__, array_size(__t__));                                                \
    } while (0)

/**
 * @brief Convenient macro for checking a test expectation.
 */
#define GIVEN(tabulator) given(tabulator)

// === CONVENIENCE FUNCTIONS FOR THE TESTS =========================================== //

static const char **expected_rows;
static uint32_t expected_row_count;

/**
 * @brief Creates an expectation for the next sequence of rows to be checked, using
 * given().
 * @see given().
 */
void expect(const char **rows, uint32_t row_count) {
    expected_rows = rows;
    expected_row_count = row_count;
}

/**
 * @brief Checks the specified tabulator against the expected results.
 */
void given(tabulator_t tabulator) {
    char buffer[256] = {0};

    tabulator_iterator_t iterator = tabulator_iterator_create(tabulator);
    TEST_ASSERT_NOT_NULL_MESSAGE(iterator, "The tabulator iterator should not be null.");

    for (int i = 0; i < expected_row_count; i++) {
        TEST_ASSERT_TRUE_MESSAGE(
            tabulator_iterator_has_next(iterator),
            "The iterator is expected to have at least one more row.");

        tabulator_row_t row = tabulator_iterator_get_next(iterator);
        TEST_ASSERT_NOT_NULL_MESSAGE(row, "The tabulator row should not be null.");

        tabulator_row_print(row, buffer, COLUMN_WIDTH, COLUMN_DELIMITER, ROW_DELIMITER,
                            CROSS_CHARACTER);

        TEST_ASSERT_EQUAL_STRING(expected_rows[i], buffer);
    }

    TEST_ASSERT_FALSE_MESSAGE(tabulator_iterator_has_next(iterator),
                              "The iterator is expected to not have any more rows.");

    tabulator_iterator_destroy(iterator);
}

// === TESTS ========================================================================= //

static tabulator_t tabulator;

SET_UP {
    tabulator = NULL;
}

TEAR_DOWN {
    if (tabulator != NULL) {
        tabulator_destroy(tabulator);
    }
}

TEST_SHOULD(create_tabulator) {
    string_t *columns;
    uint32_t column_count;
    allocate_dynamically(columns, column_count, {as_string("Column 1")});

    tabulator = tabulator_create(columns, column_count);
    TEST_ASSERT_NOT_NULL_MESSAGE(tabulator, "The tabulator should not be null.");
}

TEST_SHOULD(return_iterator_with_header_row) {
    string_t *columns;
    uint32_t column_count;
    allocate_dynamically(columns, column_count,
                         {
                             as_string("Column 1"),
                             as_string("Middle"),
                             as_string("Column 3"),
                         });

    tabulator = tabulator_create(columns, column_count);

    EXPECT({"|Column 1        |Middle          |Column 3        |\n"});
    GIVEN(tabulator);
}

TEST_SHOULD(add_row_to_tabulator) {
    string_t *columns;
    uint32_t column_count;
    allocate_dynamically(columns, column_count,
                         {
                             as_string("Column 1"),
                             as_string("Column 2"),
                         });

    string_t *row;
    allocate_dynamically(row, column_count,
                         {
                             as_string("Column Value 1"),
                             as_string("Column Value 2"),
                         });

    tabulator = tabulator_create(columns, column_count);
    tabulator_add_row(tabulator, NULL, column_count);
    tabulator_add_row(tabulator, row, column_count);

    EXPECT({
        "|Column 1        |Column 2        |\n",
        "|----------------+----------------|\n",
        "|Column Value 1  |Column Value 2  |\n",
    });
    GIVEN(tabulator);
}

TEST_SHOULD(add_multiple_rows_to_tabulator) {
    string_t *columns;
    uint32_t column_count;
    allocate_dynamically(columns, column_count,
                         {
                             as_string("Column 1"),
                             as_string("Column 2"),
                             as_string("Column 3"),
                         });

    string_t *row_1;
    allocate_dynamically(row_1, column_count,
                         {
                             as_string("Column Value 1"),
                             as_string("Column Value 2"),
                             as_string("Column Value 3"),
                         });

    string_t *row_2;
    allocate_dynamically(row_2, column_count,
                         {
                             as_string("Column Value 4"),
                             as_string("Column Value 5"),
                             as_string("Column Value 6"),
                         });

    tabulator = tabulator_create(columns, column_count);
    tabulator_add_row(tabulator, NULL, column_count);
    tabulator_add_row(tabulator, row_1, column_count);
    tabulator_add_row(tabulator, row_2, column_count);
    tabulator_add_row(tabulator, NULL, column_count);

    EXPECT({
        "|Column 1        |Column 2        |Column 3        |\n",
        "|----------------+----------------+----------------|\n",
        "|Column Value 1  |Column Value 2  |Column Value 3  |\n",
        "|Column Value 4  |Column Value 5  |Column Value 6  |\n",
        "|----------------+----------------+----------------|\n",
    });
    GIVEN(tabulator);
}

TEST_SHOULD(clear_tabulator) {
    string_t *columns;
    uint32_t column_count;
    allocate_dynamically(columns, column_count,
                         {
                             as_string("Column 1"),
                             as_string("Column 2"),
                         });

    string_t *row_1;
    allocate_dynamically(row_1, column_count,
                         {
                             as_string("Column Value 1"),
                             as_string("Column Value 2"),
                         });

    string_t *row_2;
    allocate_dynamically(row_2, column_count,
                         {
                             as_string("Column Value 4"),
                             as_string("Column Value 5"),
                         });

    tabulator = tabulator_create(columns, column_count);
    tabulator_add_row(tabulator, NULL, column_count);
    tabulator_add_row(tabulator, row_1, column_count);
    tabulator_add_row(tabulator, row_2, column_count);
    tabulator_add_row(tabulator, NULL, column_count);

    TEST_ASSERT_EQUAL(4, tabulator_clear(tabulator));

    EXPECT({"|Column 1        |Column 2        |\n"});
    GIVEN(tabulator);
}

TEST_SHOULD(add_rows_after_clearing) {
    string_t *columns;
    uint32_t column_count;
    allocate_dynamically(columns, column_count,
                         {
                             as_string("Column 1"),
                             as_string("Column 2"),
                         });

    string_t *row_1;
    allocate_dynamically(row_1, column_count,
                         {
                             as_string("Column Value 1"),
                             as_string("Column Value 2"),
                         });

    string_t *row_2;
    allocate_dynamically(row_2, column_count,
                         {
                             as_string("Column Value 4"),
                             as_string("Column Value 5"),
                         });

    tabulator = tabulator_create(columns, column_count);
    tabulator_add_row(tabulator, NULL, column_count);
    tabulator_add_row(tabulator, row_1, column_count);

    TEST_ASSERT_EQUAL(2, tabulator_clear(tabulator));

    tabulator_add_row(tabulator, row_2, column_count);
    tabulator_add_row(tabulator, NULL, column_count);

    EXPECT({
        "|Column 1        |Column 2        |\n",
        "|Column Value 4  |Column Value 5  |\n",
        "|----------------+----------------|\n",
    });
    GIVEN(tabulator);
}