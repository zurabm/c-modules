#include "linked_list.h"
#include "test.h"
#include "unity.h"

#include <malloc.h>

// === DUMMY STRUCT FOR THE TESTS ==================================================== //

typedef struct pair_t {
    uint8_t first;
    uint8_t second;
} pair_t;

pair_t *pair_create(uint8_t first, uint8_t second) {
    pair_t *pair = allocate(pair_t);
    pair->first = first;
    pair->second = second;
}

uint8_t pair_compare(const void *pair_1_ref, const void *pair_2_ref) {
    pair_t *pair_1 = (pair_t *)pair_1_ref;
    pair_t *pair_2 = (pair_t *)pair_2_ref;

    if (pair_1->first < pair_2->first) {
        return -1;
    } else if (pair_1->first == pair_2->first) {
        if (pair_1->second < pair_2->second) {
            return -1;
        } else if (pair_1->second == pair_2->second) {
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

void pair_free(void *test_struct) {
    free(test_struct);
}

// === CONVENIENCE FUNCTIONS FOR THE TESTS =========================================== //

static const pair_t *expected_pair;

/**
 * @brief Creates an expectation for the next pair to be checked, using given().
 * @see given().
 */
void expect(const pair_t *pair) {
    expected_pair = pair;
}

/**
 * @brief Creates an expectation that the next pair should be null.
 * @see given().
 */
void expect_none() {
    expected_pair = NULL;
}

/**
 * @brief Checks the specified pair against the expected results.
 */
void given(const pair_t *found_pair) {
    if (expected_pair == NULL) {
        TEST_ASSERT_NULL_MESSAGE(found_pair, "A pair was not expected to be retrieved.");
    } else {
        TEST_ASSERT_NOT_NULL_MESSAGE(found_pair, "A pair was expected to be retrieved.");

        TEST_ASSERT_EQUAL_UINT8(expected_pair->first, found_pair->first);
        TEST_ASSERT_EQUAL_UINT8(expected_pair->second, found_pair->second);
    }
}

/**
 * @brief Checks the next element from the specified iterator against the expected
 * results.
 */
void given_iterator(linked_list_iterator_t iterator) {
    if (expected_pair == NULL) {
        TEST_ASSERT_FALSE_MESSAGE(linked_list_iterator_has_next(iterator),
                                  "The iterator should not have any more elements.");
        linked_list_iterator_destroy(iterator);
    } else {
        TEST_ASSERT_TRUE_MESSAGE(linked_list_iterator_has_next(iterator),
                                 "The iterator should have one more element.");
        given(linked_list_iterator_get_next(iterator));
    }
}

// === TESTS ========================================================================= //

static linked_list_t linked_list;

SET_UP {
    linked_list = linked_list_create(pair_compare, pair_free);
}

TEAR_DOWN {
    linked_list_destroy(linked_list);
}

TEST_SHOULD(create_linked_list) {
    TEST_ASSERT_NOT_NULL_MESSAGE(linked_list, "The linked list should not be null.");
}

TEST_SHOULD(add_one_element) {
    pair_t *pair = allocate(pair_t);
    linked_list_add(linked_list, pair);
}

TEST_SHOULD(find_one_element) {
    pair_t *pair = pair_create(10, 15);
    linked_list_add(linked_list, pair);

    pair_t *different_pair = pair_create(0, 15);

    pair_t *found_pair = linked_list_find(linked_list, pair);
    pair_t *other_pair = linked_list_find(linked_list, different_pair);

    expect(pair);
    given(found_pair);

    expect_none();
    given(other_pair);

    free(different_pair);
}

TEST_SHOULD(add_multiple_elements) {
    for (int i = 0; i < 10; i++) {
        pair_t *pair = allocate(pair_t);
        linked_list_add(linked_list, pair);
    }
}

TEST_SHOULD(retrieve_multiple_elements) {
    for (int i = 0; i < 10; i++) {
        pair_t *pair = pair_create(i, 2 * i);
        linked_list_add(linked_list, pair);
    }

    for (int i = 9; i >= 10; i--) {
        pair_t different_pair = {.first = i, .second = 2 * i};
        given(linked_list_find(linked_list, &different_pair));
    }
}

TEST_SHOULD(have_one_element_in_iterator) {
    pair_t *pair = pair_create(10, 15);
    pair_t different_pair = {.first = 10, .second = 15};

    linked_list_add(linked_list, pair);

    linked_list_iterator_t iterator = linked_list_iterator_create(linked_list);

    expect(&different_pair);
    given_iterator(iterator);

    expect_none();
    given_iterator(iterator);
}

TEST_SHOULD(have_multiple_elements_in_iterator) {
    for (int i = 0; i < 10; i++) {
        pair_t *pair = pair_create(10 + i, 20 + i);
        linked_list_add(linked_list, pair);
    }

    linked_list_iterator_t iterator = linked_list_iterator_create(linked_list);

    for (int i = 0; i < 10; i++) {
        pair_t different_pair = {.first = 10 + i, .second = 20 + i};

        expect(&different_pair);
        given_iterator(iterator);
    }

    expect_none();
    given_iterator(iterator);
}

TEST_SHOULD(remove_first_element) {
    pair_t target_pair = {.first = 10, .second = 20};
    linked_list_add(linked_list, pair_create(10, 20));
    linked_list_add(linked_list, pair_create(10, 10));
    linked_list_add(linked_list, pair_create(20, 15));

    pair_t *removed_pair = linked_list_remove(linked_list, &target_pair);

    expect(&target_pair);
    given(removed_pair);

    expect_none();
    given(linked_list_find(linked_list, &target_pair));

    pair_free(removed_pair);
}

TEST_SHOULD(remove_middle_element) {
    pair_t target_pair = {.first = 10, .second = 10};
    linked_list_add(linked_list, pair_create(10, 20));
    linked_list_add(linked_list, pair_create(10, 10));
    linked_list_add(linked_list, pair_create(20, 15));

    pair_t *removed_pair = linked_list_remove(linked_list, &target_pair);

    expect(&target_pair);
    given(removed_pair);

    expect_none();
    given(linked_list_find(linked_list, &target_pair));

    pair_free(removed_pair);
}

TEST_SHOULD(remove_all_elements) {
    for (int i = 0; i < 10; i++) {
        pair_t *pair = allocate(pair_t);

        pair->first = 10 + i;
        pair->second = 20 + i;

        linked_list_add(linked_list, pair);
    }

    for (int i = 9; i >= 0; i--) {
        pair_t target_pair = {.first = 10 + i, .second = 20 + i};

        pair_t *removed_pair = linked_list_remove(linked_list, &target_pair);

        expect(&target_pair);
        given(removed_pair);

        expect_none();
        given(linked_list_find(linked_list, &target_pair));

        pair_free(removed_pair);
    }
}

TEST_SHOULD(add_removed_element) {
    pair_t target_pair = {.first = 10, .second = 20};
    linked_list_add(linked_list, pair_create(10, 20));
    linked_list_add(linked_list, pair_create(10, 10));
    linked_list_add(linked_list, pair_create(20, 15));

    linked_list_add(linked_list, linked_list_remove(linked_list, &target_pair));

    expect(&target_pair);
    given(linked_list_find(linked_list, &target_pair));
}

TEST_SHOULD(return_correct_element_count) {
    TEST_ASSERT_EQUAL(0, linked_list_size(linked_list));

    pair_t target_pair = {.first = 10, .second = 20};

    linked_list_add(linked_list, pair_create(10, 20));
    TEST_ASSERT_EQUAL(1, linked_list_size(linked_list));

    linked_list_add(linked_list, pair_create(10, 10));
    TEST_ASSERT_EQUAL(2, linked_list_size(linked_list));

    linked_list_add(linked_list, pair_create(20, 15));
    TEST_ASSERT_EQUAL(3, linked_list_size(linked_list));

    pair_t *removed_pair = linked_list_remove(linked_list, &target_pair);
    TEST_ASSERT_EQUAL(2, linked_list_size(linked_list));

    linked_list_add(linked_list, pair_create(20, 15));
    linked_list_add(linked_list, pair_create(20, 15));
    linked_list_add(linked_list, pair_create(20, 15));
    TEST_ASSERT_EQUAL(5, linked_list_size(linked_list));

    pair_free(removed_pair);
}