#include "array.h"
#include "hash_map.h"
#include "string_wrapper.h"
#include "test.h"
#include "unity.h"

TEST_FILE("linked_list.c")

#define INIT_HASH_MAP_SIZE 1

// === WRAPPER FUNCTIONS FOR THE TESTS =============================================== //

uint8_t string_compare_wrapper(const void *string_1, const void *string_2) {
    return string_compare(string_1, string_2);
}

uint32_t string_hash_wrapper(const void *string) {
    return string_hash(string);
}

void string_destroy_wrapper(void *string) {
    return string_destroy(string);
}

// === CONVENIENCE FUNCTIONS FOR THE TESTS =========================================== //

static const void *expected_key;
static const void *expected_value;

/**
 * @brief Creates an expectation for the next key-value pair to be checked, using given().
 * @see given().
 */
void expect(const void *key, const void *value) {
    expected_key = key;
    expected_value = value;
}

/**
 * @brief Checks the specified hash map entry against the expected results.
 */
void given(const_hash_map_entry_t entry) {
    TEST_ASSERT_NOT_NULL(entry);
    string_t removed_key = hash_map_entry_key(entry);
    string_t removed_value = hash_map_entry_value(entry);

    TEST_ASSERT_NOT_NULL_MESSAGE(removed_key, "The key should not be null.");
    TEST_ASSERT_NOT_NULL_MESSAGE(removed_value, "The value should not be null.");

    TEST_ASSERT_EQUAL(0, string_compare(expected_key, removed_key));
    TEST_ASSERT_EQUAL(0, string_compare(expected_value, removed_value));
}

// === TESTS ========================================================================= //

static hash_map_t hash_map;

SET_UP {
    hash_map = hash_map_create(INIT_HASH_MAP_SIZE, string_compare_wrapper,
                               string_hash_wrapper, string_destroy_wrapper,
                               string_destroy_wrapper);
}

TEAR_DOWN {
    hash_map_destroy(hash_map);
}

TEST_SHOULD(create_hash_map) {
    TEST_ASSERT_NOT_NULL_MESSAGE(hash_map, "The hash map should not be null.");
}

TEST_SHOULD(not_have_any_keys) {
    string_t test_keys[] = {
        as_string("Hello There"),
        as_string("Another One"),
        as_string("Third time's the charm"),
    };

    for (int i = 0; i < array_size(test_keys); i++) {
        TEST_ASSERT_FALSE_MESSAGE(hash_map_has_key(hash_map, test_keys[i]),
                                  "The hash map should be empty.");
    }

    for (int i = 0; i < array_size(test_keys); i++) {
        string_destroy(test_keys[i]);
    }
}

TEST_SHOULD(add_entries) {
    string_t test_keys[] = {
        as_string("Hello There"),
        as_string("Another One"),
        as_string("Third time's the charm"),
    };

    string_t test_values[] = {
        as_string("Hello There"),
        as_string("Another One"),
        as_string("Third time's the charm"),
    };

    for (int i = 0; i < array_size(test_keys); i++) {
        hash_map_put_value(hash_map, test_keys[i], test_values[i]);
    }
}

TEST_SHOULD(contain_one_entry) {
    string_t test_key = as_string("Hello There");
    string_t test_value = as_string("Another One");

    hash_map_put_value(hash_map, test_key, test_value);

    TEST_ASSERT_TRUE(hash_map_has_key(hash_map, test_key));
    TEST_ASSERT_FALSE(hash_map_has_key(hash_map, test_value));
}

TEST_SHOULD(retreive_correct_value) {
    string_t test_key = as_string("Hello There");
    string_t test_value = as_string("Another One");

    hash_map_put_value(hash_map, test_key, test_value);

    TEST_ASSERT_EQUAL(test_value, hash_map_get_value(hash_map, test_key));
}

TEST_SHOULD(retreive_correct_value_with_different_key_instance) {
    string_t test_key_1 = as_string("Hello There");
    string_t test_key_2 = as_string("Hello There");
    string_t test_value = as_string("Another One");

    hash_map_put_value(hash_map, test_key_1, test_value);

    TEST_ASSERT_EQUAL(test_value, hash_map_get_value(hash_map, test_key_2));

    string_destroy(test_key_2);
}

TEST_SHOULD(replace_value) {
    string_t test_key = as_string("Hello There");
    string_t test_value_1 = as_string("Another One");
    string_t test_value_2 = as_string("Third");

    hash_map_put_value(hash_map, test_key, test_value_1);
    TEST_ASSERT_EQUAL(test_value_1, hash_map_get_value(hash_map, test_key));

    hash_map_put_value(hash_map, test_key, test_value_2);
    TEST_ASSERT_EQUAL(test_value_2, hash_map_get_value(hash_map, test_key));
}

TEST_SHOULD(contain_multiple_keys) {
    string_t test_keys[] = {
        as_string("Hello There"),
        as_string("Another One"),
        as_string("Third time's the charm"),
    };

    string_t test_values[] = {
        as_string("Hello There"),
        as_string("Another One"),
        as_string("Third time's the charm"),
    };

    for (int i = 0; i < array_size(test_keys); i++) {
        hash_map_put_value(hash_map, test_keys[i], test_values[i]);
        TEST_ASSERT_TRUE(hash_map_has_key(hash_map, test_keys[i]));
    }

    for (int i = 0; i < array_size(test_keys); i++) {
        TEST_ASSERT_TRUE(hash_map_has_key(hash_map, test_keys[i]));
    }
}

TEST_SHOULD(retrieve_multiple_correct_values) {
    string_t test_keys[] = {
        as_string("Hello There"),
        as_string("Another One"),
        as_string("Third time's the charm"),
    };

    string_t test_values[] = {
        as_string("Hello There"),
        as_string("Another One"),
        as_string("Third time's the charm"),
    };

    for (int i = 0; i < array_size(test_keys); i++) {
        hash_map_put_value(hash_map, test_keys[i], test_values[i]);
        TEST_ASSERT_EQUAL(test_values[i], hash_map_get_value(hash_map, test_keys[i]));
    }

    for (int i = 0; i < array_size(test_keys); i++) {
        TEST_ASSERT_EQUAL(test_values[i], hash_map_get_value(hash_map, test_keys[i]));
    }
}

TEST_SHOULD(retrieve_multiple_correct_values_with_different_key_instances) {
    string_t test_keys_1[] = {
        as_string("Hello There"),
        as_string("Another One"),
        as_string("Third time's the charm"),
    };

    string_t test_keys_2[] = {
        as_string("Hello There"),
        as_string("Another One"),
        as_string("Third time's the charm"),
    };

    string_t test_values[] = {
        as_string("Hello There"),
        as_string("Another One"),
        as_string("Third time's the charm"),
    };

    for (int i = 0; i < array_size(test_keys_1); i++) {
        hash_map_put_value(hash_map, test_keys_1[i], test_values[i]);
        TEST_ASSERT_EQUAL(test_values[i], hash_map_get_value(hash_map, test_keys_1[i]));
    }

    for (int i = 0; i < array_size(test_keys_2); i++) {
        TEST_ASSERT_EQUAL(test_values[i], hash_map_get_value(hash_map, test_keys_2[i]));
        string_destroy(test_keys_2[i]);
    }
}

TEST_SHOULD(replace_multiple_values) {
    string_t test_keys[] = {
        as_string("Hello There"),
        as_string("Another One"),
        as_string("Third time's the charm"),
    };

    string_t test_values_1[] = {
        as_string("Value"),
        as_string("One"),
        as_string("Third"),
    };

    string_t test_values_2[] = {
        as_string("Another Value"),
        as_string("Another One"),
        as_string("Fourth"),
    };

    for (int i = 0; i < array_size(test_keys); i++) {
        hash_map_put_value(hash_map, test_keys[i], test_values_1[i]);
        TEST_ASSERT_EQUAL(test_values_1[i], hash_map_get_value(hash_map, test_keys[i]));

        hash_map_put_value(hash_map, test_keys[i], test_values_2[i]);
        TEST_ASSERT_EQUAL(test_values_2[i], hash_map_get_value(hash_map, test_keys[i]));
    }
}

TEST_SHOULD(remove_value) {
    string_t target_key = as_string("To Be Removed");
    string_t target_value = as_string("Value 2");

    hash_map_put_value(hash_map, as_string("Key 1"), as_string("Value 1"));
    hash_map_put_value(hash_map, target_key, target_value);
    hash_map_put_value(hash_map, as_string("Key 3"), as_string("Value 3"));

    hash_map_entry_t entry = hash_map_remove_key(hash_map, target_key);

    expect(target_key, target_value);
    given(entry);

    hash_map_entry_destroy(entry, 1, 1);
}

TEST_SHOULD(remove_all_values) {
    string_t test_keys[] = {
        as_string("Key 1"),
        as_string("Key 2"),
        as_string("Key 3"),
    };

    string_t test_values[] = {
        as_string("Value 1"),
        as_string("Value 2"),
        as_string("Value 3"),
    };

    for (int i = 0; i < array_size(test_keys); i++) {
        hash_map_put_value(hash_map, test_keys[i], test_values[i]);
    }

    for (int i = 0; i < array_size(test_keys); i++) {
        hash_map_entry_t entry = hash_map_remove_key(hash_map, test_keys[i]);

        expect(test_keys[i], test_values[i]);
        given(entry);

        hash_map_entry_destroy(entry, 1, 1);
    }
}

TEST_SHOULD(add_removed_value) {
    string_t target_key = as_string("To Be Removed");
    string_t target_value = as_string("Value 2");

    hash_map_put_value(hash_map, as_string("Key 1"), as_string("Value 1"));
    hash_map_put_value(hash_map, target_key, target_value);
    hash_map_put_value(hash_map, as_string("Key 3"), as_string("Value 3"));

    hash_map_entry_t entry = hash_map_remove_key(hash_map, target_key);

    expect(target_key, target_value);
    given(entry);

    hash_map_put_value(hash_map, hash_map_entry_key(entry), hash_map_entry_value(entry));

    TEST_ASSERT_EQUAL(target_value, hash_map_get_value(hash_map, target_key));

    hash_map_entry_destroy(entry, 0, 0);
}

TEST_SHOULD(add_multiple_removed_values) {
    string_t test_keys[] = {
        as_string("Key 1"),
        as_string("Key 2"),
        as_string("Key 3"),
    };

    string_t test_values[] = {
        as_string("Value 1"),
        as_string("Value 2"),
        as_string("Value 3"),
    };

    for (int i = 0; i < array_size(test_keys); i++) {
        hash_map_put_value(hash_map, test_keys[i], test_values[i]);
    }

    for (int i = 0; i < array_size(test_keys); i++) {
        hash_map_entry_t entry = hash_map_remove_key(hash_map, test_keys[i]);
        hash_map_entry_destroy(entry, 0, 0);
    }

    for (int i = 0; i < array_size(test_keys); i++) {
        hash_map_put_value(hash_map, test_keys[i], test_values[i]);
        TEST_ASSERT_EQUAL(test_values[i], hash_map_get_value(hash_map, test_keys[i]));
    }
}

TEST_SHOULD(return_correct_size) {
    string_t test_keys[] = {
        as_string("Key 1"), as_string("Key 2"), as_string("Key 3"),
        as_string("Key 4"), as_string("Key 5"),
    };

    string_t test_values[] = {
        as_string("Value 1"), as_string("Value 2"), as_string("Value 3"),
        as_string("Value 4"), as_string("Value 5"),
    };

    for (int i = 0; i < array_size(test_keys); i++) {
        TEST_ASSERT_EQUAL(i, hash_map_size(hash_map));

        hash_map_put_value(hash_map, test_keys[i], test_values[i]);
    }

    for (int i = 0; i < array_size(test_keys); i++) {
        TEST_ASSERT_EQUAL(array_size(test_keys) - i, hash_map_size(hash_map));

        hash_map_entry_t entry = hash_map_remove_key(hash_map, test_keys[i]);
        hash_map_entry_destroy(entry, 0, 0);
    }

    for (int i = 0; i < array_size(test_keys); i++) {
        TEST_ASSERT_EQUAL(i, hash_map_size(hash_map));

        hash_map_put_value(hash_map, test_keys[i], test_values[i]);
    }
}

TEST_SHOULD(return_same_size_when_replacing_keys) {
    string_t test_keys[] = {
        as_string("Key 1"), as_string("Key 2"), as_string("Key 3"),
        as_string("Key 4"), as_string("Key 5"),
    };

    string_t test_values_1[] = {
        as_string("Value 1"), as_string("Value 2"), as_string("Value 3"),
        as_string("Value 4"), as_string("Value 5"),
    };

    string_t test_values_2[] = {
        as_string("Value 5"), as_string("Value 4"), as_string("Value 3"),
        as_string("Value 2"), as_string("Value 1"),
    };

    for (int i = 0; i < array_size(test_keys); i++) {
        hash_map_put_value(hash_map, test_keys[i], test_values_1[i]);
    }

    for (int i = 0; i < array_size(test_keys); i++) {
        hash_map_put_value(hash_map, test_keys[i], test_values_2[i]);

        TEST_ASSERT_EQUAL(array_size(test_keys), hash_map_size(hash_map));
    }
}