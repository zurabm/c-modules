#include "options_parser.h"
#include "test.h"
#include "unity.h"

TEST_FILE("hash_map.c")
TEST_FILE("linked_list.c")
TEST_FILE("string_wrapper.c")
TEST_FILE("tokenizer.c")

// === CONVENIENCE FUNCTIONS FOR THE TESTS =========================================== //

static const char *expected_value;
static uint32_t expected_value_length;

/**
 * @brief Expects the specified value for an option. Used with given().
 * @see given().
 */
void expect(const char *value, uint32_t value_length) {
    expected_value = value;
    expected_value_length = value_length;
}

/**
 * @brief Expects the following option to not be present. Used with given().
 * @see given().
 */
void expect_none() {
    expected_value = NULL;
}

/**
 * @brief Checks the specified option against the expected results.
 */
void given(options_parser_t options_parser, const char *option) {
    if (expected_value == NULL) {
        TEST_ASSERT_FALSE(options_parser_has_option(options_parser, option));
    } else {
        TEST_ASSERT_TRUE(options_parser_has_option(options_parser, option));

        uint32_t value_length;
        const char *value = options_parser_get_option(options_parser, option,
                                                      &value_length);

        TEST_ASSERT_EQUAL_CHAR_ARRAY(expected_value, value, expected_value_length);
        TEST_ASSERT_EQUAL_UINT32(value_length, expected_value_length);
    }
}

// === TESTS ========================================================================= //

static options_parser_t options_parser;

TEAR_DOWN {
    options_parser_destroy(options_parser);
}

TEST_SHOULD(create_options_parser) {
    const char options[] = "command -option1 value1 -option2 value2 -option3 value3";
    uint32_t options_length = sizeof(options) -
                              1; // -1 as to not include the \0 character.

    options_parser = options_parser_create(options, options_length, '-', ' ');

    TEST_ASSERT_NOT_NULL_MESSAGE(options_parser,
                                 "The options parser should not be null.");
}

TEST_SHOULD(find_from_one_option) {
    const char options[] = "command -option1 value1";
    uint32_t options_length = sizeof(options) -
                              1; // -1 as to not include the \0 character.

    options_parser = options_parser_create(options, options_length, '-', ' ');

    TEST_ASSERT_TRUE(options_parser_has_option(options_parser, "option1"));
    TEST_ASSERT_FALSE(options_parser_has_option(options_parser, "option2"));
    TEST_ASSERT_FALSE(options_parser_has_option(options_parser, "option3"));
    TEST_ASSERT_FALSE(options_parser_has_option(options_parser, "unknown"));
}

TEST_SHOULD(parse_one_option) {
    const char options[] = "command -option1 value1";
    uint32_t options_length = sizeof(options) - 1;

    options_parser = options_parser_create(options, options_length, '-', ' ');

    expect("value1", sizeof("value1") - 1);
    given(options_parser, "option1");
}

TEST_SHOULD(find_from_multiple_option) {
    const char options[] = "command -option1 value1 -option2 value2 -option3 value3";
    uint32_t options_length = sizeof(options) -
                              1; // -1 as to not include the \0 character.

    options_parser = options_parser_create(options, options_length, '-', ' ');

    TEST_ASSERT_TRUE(options_parser_has_option(options_parser, "option1"));
    TEST_ASSERT_TRUE(options_parser_has_option(options_parser, "option2"));
    TEST_ASSERT_TRUE(options_parser_has_option(options_parser, "option3"));
    TEST_ASSERT_FALSE(options_parser_has_option(options_parser, "unknown"));
}

TEST_SHOULD(parse_multiple_options) {
    const char options[] = "command -option1 value1 -option2 value2 -option3 value3";
    uint32_t options_length = sizeof(options) -
                              1; // -1 as to not include the \0 character.

    options_parser = options_parser_create(options, options_length, '-', ' ');

    expect("value1", sizeof("value1") - 1);
    given(options_parser, "option1");

    expect("value2", sizeof("value2") - 1);
    given(options_parser, "option2");

    expect("value3", sizeof("value3") - 1);
    given(options_parser, "option3");

    expect_none();
    given(options_parser, "unknown");
}

TEST_SHOULD(parse_options_in_any_order) {
    const char options[] = "command_+option1_value1_+option2_value2_+option3_value3";
    uint32_t options_length = sizeof(options) -
                              1; // -1 as to not include the \0 character.

    options_parser = options_parser_create(options, options_length, '+', '_');

    expect("value3", sizeof("value3") - 1);
    given(options_parser, "option3");

    expect("value2", sizeof("value2") - 1);
    given(options_parser, "option2");

    expect("value1", sizeof("value1") - 1);
    given(options_parser, "option1");

    expect("value2", sizeof("value2") - 1);
    given(options_parser, "option2");

    expect("value3", sizeof("value3") - 1);
    given(options_parser, "option3");

    expect_none();
    given(options_parser, "unknown");
}